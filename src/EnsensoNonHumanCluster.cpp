#include "ObjectCluster.h"
#include <pcl/features/moment_of_inertia_estimation.h>

#define X 0
#define Y 1
#define Z 2

class EnsensoNonHumanCluster : virtual private ObjectClusterInterface{

private:
  ros::Subscriber pc_sub;
  ros::NodeHandle nh;
  ros::Publisher pc_pub;
  ros::Publisher vis_pub;
  ros::Publisher cluster_pub;  

public:
  explicit EnsensoNonHumanCluster(ros::NodeHandle& n):
    nh(n) {
    pc_sub = nh.subscribe("/textured_point_cloud",1,
			  &EnsensoNonHumanCluster::Clustering, this);
    cluster_pub = nh.advertise<object_recognizer::Cluster>("/cluster", 1);
    pc_pub = nh.advertise<sensor_msgs::PointCloud2>("/clustered_cloud2", 1);
    vis_pub = nh.advertise<visualization_msgs::MarkerArray>("visualization_marker",10);
  }

  void Clustering(const sensor_msgs::PointCloud2::Ptr &input){
    /** convert sensor_msgs to pcl */
    sensor_msgs::PointCloud2::Ptr input_cloud = input;
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZI>);
    pcl::fromROSMsg(*input, *cloud);

    /** remove invalid points*/
    std::vector<int> indices;
    pcl::removeNaNFromPointCloud(*cloud, *cloud, indices);
    
    /** perform downsampling for performance */
    ROS_INFO("downsampling");
    pcl::VoxelGrid<pcl::PointXYZI> sor_down;
    sor_down.setInputCloud(cloud->makeShared());
    sor_down.setLeafSize(0.01, 0.01, 0.01);
    sor_down.filter(*cloud);

    /** remove outliers by statical filtering */
    ROS_INFO("removing outliers");
    pcl::StatisticalOutlierRemoval<pcl::PointXYZI> sor_stat;
    sor_stat.setInputCloud(cloud->makeShared());
    sor_stat.setMeanK(50);
    sor_stat.setStddevMulThresh(1.0);
    sor_stat.setNegative(false);
    sor_stat.filter(*cloud);
    
    //include SAC
    // Create the segmentation object for the planar model and set all the parameters
    ROS_INFO("segmentation");
    pcl::SACSegmentation<pcl::PointXYZI> seg;
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud_plane (new pcl::PointCloud<pcl::PointXYZI> ());

    seg.setOptimizeCoefficients (true);
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (100);
    seg.setDistanceThreshold (0.20);

    int i=0, nr_points = (int) cloud->points.size ();
    while (cloud->points.size () > 0.7 * nr_points)
      {
	// Segment the largest planar component from the remaining cloud
	seg.setInputCloud (cloud);
	seg.segment (*inliers, *coefficients);
	if (inliers->indices.size () == 0)
	  {
	    std::cout << "Could not estimate a planar model for the given dataset." << std::endl;
	    break;
	  }
	
	// Extract the planar inliers from the input cloud
	pcl::ExtractIndices<pcl::PointXYZI> extract;
	extract.setInputCloud (cloud);
	extract.setIndices (inliers);
	extract.setNegative (false);

	// Get the points associated with the planar surface
	extract.filter (*cloud_plane);
	std::cout << "PointCloud representing the planar component: " << cloud_plane->points.size () << " data points." << std::endl;

	// Remove the planar inliers, extract the rest
	extract.setNegative (true);
	extract.filter (*cloud);
      }
    // Creating the KdTree object for the search method of the extraction
    pcl::search::KdTree<pcl::PointXYZI>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZI>);
    ROS_INFO("KD tree");
    tree->setInputCloud (cloud);
    ROS_INFO("setup finished");
    std::vector<pcl::PointIndices> cluster_indices;
    pcl::EuclideanClusterExtraction<pcl::PointXYZI> ec;
    ec.setClusterTolerance (0.02); // 2cm
    ec.setMinClusterSize (100);
    ec.setMaxClusterSize (25000);
    ec.setSearchMethod (tree);
    ec.setInputCloud (cloud);
    ec.extract (cluster_indices);


    int j = 0;
    std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr > cloud_cluster_list;

    for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
      {
	pcl::PointCloud<pcl::PointXYZI>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZI>);
	for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit){
	  cloud_cluster->points.push_back(cloud->points[*pit]); //*
	}
	cloud_cluster->width = cloud_cluster->points.size();
	cloud_cluster->height = 1;
	cloud_cluster->is_dense = true;
	cloud_cluster_list.push_back(cloud_cluster);

      }
    /** position */
    double pos[3] = {0.0, 0.0, 0.0};
    double lpos[3] = {0.0, 0.0, 0.0};

    object_recognizer::Cluster cluster;
    object_recognizer::Prob prob;
    object_recognizer::Info info;


    for(size_t i = 0; i < cloud_cluster_list.size(); i++){


      pcl::PointCloud<pcl::PointXYZ>::Ptr orientatedBB;
      copyPointCloud(*orientatedBB, *cloud_cluster_list[i]);
      pcl::MomentOfInertiaEstimation <pcl::PointXYZ> feature_extractor;
      feature_extractor.setInputCloud (orientatedBB);
      feature_extractor.compute ();

      pcl::PointXYZ min_point, max_point;
      
      std::vector <float> moment_of_inertia;
      std::vector <float> eccentricity;
      //pcl::PointXYZI min_point_AABB;
      //pcl::PointXYZI max_point_AABB;
      //pcl::PointXYZI min_point_OBB;
      //pcl::PointXYZI max_point_OBB;
      pcl::PointXYZ position_OBB;
      Eigen::Matrix3f rotational_matrix;
      float major_value, middle_value, minor_value;
      Eigen::Vector3f major_vector, middle_vector, minor_vector;
      Eigen::Vector3f mass_center;

      feature_extractor.getMomentOfInertia (moment_of_inertia);
      feature_extractor.getEccentricity (eccentricity);
      feature_extractor.getOBB (min_point, max_point, position_OBB, rotational_matrix);
      feature_extractor.getEigenValues (major_value, middle_value, minor_value);
      feature_extractor.getEigenVectors (major_vector, middle_vector, minor_vector);
      feature_extractor.getMassCenter (mass_center);


      Eigen::Vector3f position (position_OBB.x, position_OBB.y, position_OBB.z);
      std::cout << "position : " << position.x() << position.y() << position.z() << std::endl;
      std::cout << "size : " << max_point.x - min_point.x << max_point.y - min_point.y << max_point.z - min_point.z << std::endl;
      std::cout << "centroid : " << mass_center.x() << mass_center.y() << mass_center.z() << std::endl;
      
      Eigen::Quaternionf quat (rotational_matrix);
      
      /** get max/min */
      // pcl::PointXYZI min_point, max_point; 
      //pcl::getMinMax3D(cloud_cluster_list[i], min_point, max_point);

      Eigen::Vector3f centroid;
      centroid = mass_center;
      //compute3DCentroid(cloud_cluster_list[i], centroid);
      
      /** get size */
      double x = (max_point.x - min_point.x);
      double y = (max_point.y - min_point.y);
      double z = (max_point.z - min_point.z);

      std::cout << "x y z" << std::endl;
      std::cout << x << " " << y << " " << z << std::endl;
      /** update position */
      lpos[X] = pos[X];
      lpos[Y] = pos[Y];
      lpos[Z] = pos[Z];
      pos[X] = centroid.x();
      pos[Y] = centroid.y();
      pos[Z] = centroid.z();
      
      /** setup probability */
      prob.all = WidthRule(x) * HeightRule(y) * DepthRule(z) * VolumeRule(x * y * z);
      prob.centroid = CentroidRule(pos);
      prob.tangent = TangentRule(y/x);
      prob.volume = VolumeRule(x*y*z);
      prob.height = HeightRule(y);
      
      std::cout << "centroid " << prob.centroid << std::endl;
      std::cout << "tangent " << prob.tangent << std::endl;
      std::cout << "volume " << prob.volume << std::endl;
      std::cout << "height " << prob.height << std::endl;

      /** setup info */
      info.volume = x * y * z;
      info.width = x;
      info.height = y;
      info.depth = z;
      info.centroid.position.x = pos[X];
      info.centroid.position.y = pos[Y];
      info.centroid.position.z = pos[Z];
      info.centroid.orientation.x = quat.x();
      info.centroid.orientation.y = quat.y(); 
      info.centroid.orientation.z = quat.z();
      info.centroid.orientation.w = quat.w();
	
      if(prob.all > 0.5){
	/** set data*/
	std::cout << "entered store data" << std::endl;
	cluster.prob.push_back(prob);
	cluster.info.push_back(info);
	cluster.width.push_back(x);
	cluster.height.push_back(y);
	sensor_msgs::PointCloud2 cluster_cloud;
	sensor_msgs::Image image;
	//pcl::toROSMsg(cloud_cluster_list[i], image);
	pcl::toROSMsg(*cloud_cluster_list[i], cluster_cloud);
	cluster_cloud.header.frame_id = "ensenso_optical_frame";
	cluster.candidates.push_back(cluster_cloud);
      }
    }
    ROS_INFO("number of candidates are : %d", cluster.candidates.size());
    displayBoundingBox(cluster);
    displayCluster(cluster);
    cluster_pub.publish(cluster);
  }

  double VolumeRule(double volume){
    if(0.02 >= volume ){
      return 1.0;
    }
    else 
      return 0.0;
  }
  
  double WidthRule(double width){
    if(width >= 0.05 && 0.3 >= width)
      return 1.0;
    else if(width < 0.05)
      return width;
    else if(1.0 - width < 0)
      return 0.0;
    else
      return (1.0 - width);
  }

  double HeightRule(double height){
    if(height >= 0.05 && height <= 0.4)
      return 1.0;
    else if(height > 0.4)
      if(1.0 - height < 0)
	return 0.0;
      else
	return 1.0 - height;
    else
      return  height;
  }


  double DepthRule(double z){
    if(z >= 0.05 && 0.3 >= z)
      return 1.0;
    else if(z < 0.05)
      return z;
    else if (1.0 - z < 0)
      return 0;
    else
      return (1.0 - z);
  }

  double TangentRule(double tan){
    if(tan >= 1.5 && 7.0 >= tan){
      return 1.0;
    }
    else if(tan < 1.5)
      return tan/1.5;
    else
      return 0;
  }

  double KalmanFilter(double lpos[], double pos[]){
    double prob[3] = {0.0, 0.0, 0.0};
    
    double distance[3] = {0.0, 0.0, 0.0};

    distance[X] = abs(pos[X] - lpos[X]);
    distance[Y] = abs(pos[Y] - lpos[Y]);
    distance[Z] = abs(pos[Z] - lpos[Z]);

    for(int i = 0; i < 3; i++){
      if(distance[i] > 0.2)
	prob[i] = 0.0;
      else
	prob[i] = 1.0;
    }

    return prob[X] * prob[Y] * prob[Z];
  }



  double CentroidRule(double centroid[]){
    if(centroid[Y] > 0)
      return 1.0;
    else
      return 0.0;
  }

  void displayCluster(object_recognizer::Cluster cluster){
    pcl::PointCloud<pcl::PointXYZI> cluster_sum;
    pcl::PointCloud<pcl::PointXYZI> cluster_elem;
    for(size_t i = 0; i < cluster.candidates.size(); i++){
      pcl::fromROSMsg(cluster.candidates[i], cluster_elem);
      cluster_sum += cluster_elem;
    }
    sensor_msgs::PointCloud2 cluster_cloud;
    cluster_cloud.header.frame_id = "ensenso_optical_frame";
    pcl::toROSMsg(cluster_sum, cluster_cloud);
    pc_pub.publish(cluster_cloud);
  }
  
  void displayBoundingBox(object_recognizer::Cluster cluster){
    visualization_msgs::MarkerArray marker_array;

    for(size_t i = 0; i < cluster.candidates.size(); i++){
      visualization_msgs::Marker marker;
      marker.type = visualization_msgs::Marker::CUBE;
      marker.action = visualization_msgs::Marker::ADD;
      marker.header.frame_id = "ensenso_optical_frame";
      marker.header.stamp = ros::Time();

      pcl::PointXYZ min_point, max_point; 

      pcl::PointCloud<pcl::PointXYZ> cloud;

      pcl::fromROSMsg(cluster.candidates[i], cloud);

     
      marker.header.frame_id = "ensenso_optical_frame";
      marker.header.stamp = ros::Time::now();
      marker.ns = "bounding_box";
      marker.id = i;

      marker.color.a = 0.5;
      marker.color.r = cluster.prob[i].all;
      marker.color.g = 0.0;
      marker.color.b = 1.0 - cluster.prob[i].all;
      marker.scale.x = cluster.info[i].width;
      marker.scale.y = cluster.info[i].height;
      marker.scale.z = cluster.info[i].depth;
      marker.pose = cluster.info[i].centroid;
      marker_array.markers.push_back(marker); 
    }
    for(size_t i = 0; i < marker_array.markers.size(); i++){
      marker_array.markers[i].id = i;
    }

    vis_pub.publish(marker_array);
  }
  

};



