#include "LIDARObjectCluster.cpp"
#include "RGBDObjectCluster.cpp"
#include "EnsensoObjectCluster.cpp"

int main(int argc, char** argv){
  ros::init(argc, argv, "ensenso_cluster");
  
  ros::NodeHandle nh("~");
  EnsensoObjectCluster* objectcluster = new EnsensoObjectCluster(nh);
  ros::spin();
}
