#include "RGBDNonHumanCluster.cpp"
#include "EnsensoNonHumanCluster.cpp"

int main(int argc, char** argv){
  ros::init(argc, argv, "object_cluster");
  
  ros::NodeHandle nh("~");
  EnsensoNonHumanCluster* objectcluster = new EnsensoNonHumanCluster(nh);
  ros::spin();
}
