#object_recognizer  
Source code used in RoboCup@home 2016 Object Recognition/Human Recognition.  

##License
BSD-3  

##Supported sensors
Intel Realsense R200  
![realsense](https://media.rs-online.com/t_large/F1340406-01.jpg)  
HOKUYO YVT-35LX(only human detection support)  
![hokuyo](https://www.hokuyo-aut.co.jp/p/product/20150430134240_385.jpg)    

##Usage
Object recognition.  
```bash
roslaunch object_recognizer obj_recog.launch
```


Human tracking.
```bash
roslaunch object_recognizer human_track.launch
```

##ROS Node  
###object_cluster
Object Clustering Node. Visualize intermidiate results.  
![object_cluster](object_cluster.jpeg)  
###object_detector.py
Object Recognition with Convolutional Neural Network.(deprecated)
RViz marker visualization support.  
-> Currently we are using [YOLOv2](https://github.com/kendemu/yolo9000_tensorflow)  
###human_recognizer.py
Human 3D Detection with Hiearchial Clustering. Rviz visualization with humanoid 3d model.  
###human_filter.py
Track human with unique id.   
![human_track](human_track.jpeg)  


##How to support other sensors
Requirements : Have interface for PCL(RGB-D not required)  
1. Implement the interface include/ObjectClusterInterface.h.  
Code examples are src/LIDARObjectCluster.cpp, and src/RGBDObjectCluster.cpp.  
2. Publish topic /cluster, /clustered_cloud2, /visualization_marker  
3. Wrap the implemented class to ROS Node and make it executable.  
Code examples are src/object_cluster_node.cpp, and src/nonhuman_cluster_node.cpp. If you want to support LIDAR, just make the instance of  the LIDARObjectCluster class.  





